package com.armies;

import com.armies.Soldier;
import com.armies.equipment.Equipment;

/**
 * @author tristan
 * Implement the Decorator Design Pattern for Soldier
 */
public class DecoratorSoldier implements Soldier{
	private Soldier soldier; 
	private Equipment equipment;
	
	public DecoratorSoldier() {
		super();
	}
	
	public DecoratorSoldier(Soldier soldier) {
		this.soldier = soldier; 
	}
	
	public void addEquipment(Equipment equipment) {
		this.equipment = equipment;		
	}
	
	public Equipment getEquipment() {
		return this.equipment;
	}
 
	// @Override
	// public int hit() {
	// 	if (equipment != null)
	// 		return soldier.hit() + equipment.getAttackStrength();
	// 	else return soldier.hit();
	// }

	// @Override
	// public boolean defend(int force) {
	// 	if (equipment != null)
	// 		return soldier.defend(force - equipment.getDefendStrength());
	// 	return soldier.defend(force);
	// }

	@Override
	public int hit (int damage) {
		return damage;
	}

	@Override
	public boolean wardoff(int damage) {
		if (equipment != null)
			return soldier.wardoff(damage - equipment.getDefendStrength());
		return soldier.wardoff(damage);
	}
}
