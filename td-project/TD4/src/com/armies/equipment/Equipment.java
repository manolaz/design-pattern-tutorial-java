package com.armies.equipment;

public interface Equipment {
	public int getDefendStrength();
	public int getAttackStrength();
}
