package com.armies;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Group a1 = new Group();
        Group b2 = new Group();
        Infantrymen i1 = new Infantrymen();
        Infantrymen i2 = new Infantrymen();
        Horsemen h1 = new Horsemen();
        Horsemen h2 = new Horsemen();

        a1.assignSoldier(i1);
        a1.assignSoldier(h1);
        b2.assignSoldier(i2);
        b2.assignSoldier(h2);
        a1.assignSoldier(b2);
        a1.showAll();
        b2.showAll();

}}
