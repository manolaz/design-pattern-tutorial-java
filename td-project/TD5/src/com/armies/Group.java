package com.armies;

import java.util.ArrayList;
import java.util.List;

public class Group implements SoldierComponent {
    private List<SoldierComponent> army = new ArrayList<>();

    public void assignSoldier(SoldierComponent s){
        this.army.add(s);
    }

    public void hit(int damage){

    }

    public void showAll(){
        System.out.println("Army are : ");
        System.out.println(this.army);
    }
}
