package com.armies;

public interface Soldier extends SoldierComponent{
    public int hit(int damage);
    public boolean wardoff(int damage);
}
