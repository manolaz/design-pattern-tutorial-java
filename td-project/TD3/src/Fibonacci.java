
public class Fibonacci implements Algorithm {
    
    // private String _name;
    // private double _value;
    // public Fibonacci() {
    //     _name = "Fibonacci";
    //     _value = 0;
    // };

    @Override
    public String getName() {
        return "Fibonacci";
    }

    @Override
    public double getVal(int initial) {
        //create array to store Fibonacci values
//        double []fibo=new double[initial];
//        fibo[0]= 1;
//        fibo[1]=1;
//        for (int i = 2; i <= initial; i++) {
//            fibo[i]=fibo[i-1]+fibo[i-2];
//        }
//        return fibo[initial-1];
        double m1, m2;
        m1=1;
        m2=1;
        for (int i = 2; i <= initial; i++) {
            double temp = m2 + m1;
            m2 = m1;
            m1 = temp;
        }
        return m1;
    }
}