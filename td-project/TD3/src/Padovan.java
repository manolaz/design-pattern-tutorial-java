
public class Padovan implements  Algorithm {
    
    // private String _name;
    // private double _value;
    // public Padovan() {
    //     _name = "Padovan";
    //     _value = 0;
    // };

    @Override
    public String getName() {
        return "Padovan";
    }

    @Override
    public double getVal(int initial) {
        //create array to store padovan values
//        double []padv=new double[initial];
//        padv[0]=padv[1]=padv[2]=1;
//        for (int i = 2; i <= initial; i++) {
//            padv[i]=padv[i-2]+padv[i-3];
//        }
//        return padv[initial-1];
        double p0,p1,p2;
        p0 = p1 = p2 = 1;
        for (int i = 3; i < initial; i++) {
            double temp = p0 + p1;
            p0 = p1;
            p1 = p2;
            p2 = temp;
        }
        return p2;
    }
}
