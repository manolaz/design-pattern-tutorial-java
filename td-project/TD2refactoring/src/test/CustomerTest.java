package test;

import model.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CustomerTest {

  Movie[] movies;
  Rental[] rentals;
  Customer john;

  @Before
  public void beforeEachTest() {
    movies = new Movie[]{
      new Movie("Rogue One", new NewReleasePricing()),
      new Movie("Frozen", new ChildrenPricing()),
      new Movie("Star Wars III", new RegularPricing())
    };

    rentals = new Rental[]{
      new Rental(movies[0], 5),
      new Rental(movies[1], 7),
      new Rental(movies[2], 4)
    };

    john = new Customer("John");
    for (Rental r : rentals) {
      john.addRental(r);
    }
  }

  @Test
  public void statementTest_John() {
    String actual = john.statement();
    String expected = "Rental Record for John\n" +
      "\tRogue One\t15.0 \n" +
      "\tFrozen\t7.5 \n" +
      "\tStar Wars III\t5.0 \n" +
      "Amount owned is 27.5\n" +
      "You earned 4 frequent renter points";
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void statementHTMLTest_John() {
    String actual = john.statementHTML();
    String expected = "<html><body>Rental Record for John<br/>&nbsp;&nbsp;Rogue One&nbsp;&nbsp;15.0 <br/>&nbsp;&nbsp;Frozen&nbsp;&nbsp;7.5 <br/>&nbsp;&nbsp;Star Wars III&nbsp;&nbsp;5.0 <br/>Amount owned is 27.5<br/>You earned 4 frequent renter points</body></html>";
    Assert.assertEquals(expected, actual);
  }
}
