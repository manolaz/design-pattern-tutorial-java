import model.*;

import java.io.IOException;
import java.io.PrintWriter;

public class Main {

  public static void main(String[] args) {
    Movie[] movies = {
      new Movie("Rogue One", new NewReleasePricing()),
      new Movie("Frozen", new ChildrenPricing()),
      new Movie("Star Wars III", new RegularPricing())
    };

    Rental[] rentals = {
      new Rental(movies[0], 5),
      new Rental(movies[1], 7),
      new Rental(movies[2], 4)
    };

    Customer c = new Customer("John");
    for (Rental r : rentals) {
      c.addRental(r);
    }

    String ret = c.statement();
    System.out.println(ret);

//    String html = c.statementHTML();
//    try {
//      String fileName = "index.html";
//      PrintWriter pw = new PrintWriter(fileName);
//      pw.println(html);
//      pw.close();
//      System.out.println("write done.");
//    } catch (IOException e) {
//      System.out.println(e.getMessage());
//    }
  }
}
