import org.junit.Assert;
import org.junit.Test;

public class NumberTest {

  @Test
  public void Sum_6_9_15() {
    final int expected = 15;
    final int actual = Number.Sum(6, 9);
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void Sum_0_9_9() {
    final int expected = 9;
    final int actual = Number.Sum(0, 9) ;
    Assert.assertEquals(expected, actual);
  }
}
