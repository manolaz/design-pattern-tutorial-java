package model;

public class MoviePricing {
  protected double basePrice;
  protected int baseDays;
  protected double extendedPricePerDays;

  public MoviePricing(double basePrice, int baseDays, double extendedPricePerDays) {
    this.basePrice = basePrice;
    this.baseDays = baseDays;
    this.extendedPricePerDays = extendedPricePerDays;
  }

  public double getAmount(int days) {
    double amount = basePrice;
    if (days > baseDays)
      amount += (days - baseDays) * extendedPricePerDays;

    return amount;
  }

  public int getRentalPts(int days) {
    return 1;
  }
}
