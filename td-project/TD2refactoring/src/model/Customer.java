package model;

import java.util.*;

public class Customer {
  private String _name;
  private ArrayList<Rental> _rentals = new ArrayList<>();

  public Customer(String name) {
    _name = name;
  }

  public void addRental(Rental rental) {
    _rentals.add(rental);
  }

  public String getName() {
    return _name;
  }

  private double getTotalAmount() {
    double totalAmount = 0;
    for (Rental each : this._rentals) {
      totalAmount += each.getRentalAmount();
    }

    return totalAmount;
  }

  private int getRentalPts() {
    int pts = 0;
    for (Rental each : this._rentals) {
      pts += each.getRentalPts();
    }

    return pts;
  }

  public String statement() {
    return statement("\n", "\t");
  }

  public String statementHTML() {
    return String.format(
      "<html><body>%s</body></html>",
      statement("<br/>", "&nbsp;&nbsp;")
    );
  }

  private String statement(String NEWLINE, String TAB) {
    String result = "Rental Record for " + getName() + NEWLINE;
    for (Rental each : this._rentals) {
      result += TAB + each.getMovie().getTitle() + TAB + each.getRentalAmount() + " " + NEWLINE;
    }

    result += "Amount owned is " + getTotalAmount() + NEWLINE;
    result += "You earned " + getRentalPts() + " frequent renter points";
    return result;
  }
}
 