package model;

public class RegularPricing extends MoviePricing {
  public RegularPricing() {
    super(2, 2, 1.5);
  }
}
