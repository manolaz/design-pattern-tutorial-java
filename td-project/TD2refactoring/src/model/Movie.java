package model;

public class Movie {
  private String _title;
  private MoviePricing _priceCode;

  public Movie(String title, MoviePricing priceCode) {
    _title = title;
    _priceCode = priceCode;
  }

  public MoviePricing getPriceCode() {
    return _priceCode;
  }

  public void setPriceCode(MoviePricing priceCode) {
    _priceCode = priceCode;
  }

  public String getTitle() {
    return _title;
  }

}
