package model;

public class NewReleasePricing extends MoviePricing {

  public NewReleasePricing() {
    super(0, 0, 0);
  }

  @Override
  public double getAmount(int days) {
    return days * 3;
  }

  @Override
  public int getRentalPts(int days) {
    if (days > 1) return 2;
    return 1;
  }
}
