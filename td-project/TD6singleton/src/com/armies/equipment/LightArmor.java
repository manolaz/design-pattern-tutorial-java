package com.armies.equipment;

public class LightArmor extends Armor {

	@Override
	public int getDefendStrength() {
		return 2;
	}

}
