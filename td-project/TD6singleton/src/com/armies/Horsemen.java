package com.armies;

public class Horsemen implements Soldier{
    private int health = 50;

    @Override
    public int getHealth() {
        return this.health;
    }

    @Override
    public int hit(int damage) {
        return damage;
    }

    @Override
    public boolean wardoff(int damage) {
        this.health -= damage;
        return this.health != 0;
    }
}
