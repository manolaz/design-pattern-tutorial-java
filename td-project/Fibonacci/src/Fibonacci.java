public class Fibonacci implements Algorithm {
    public Fibonacci() {
    };

    @Override
    public String getName() {
        return "Fibonacci";
    }

    @Override
    public double getVal(int initial) {
        //create array to store Fibonacci values
        int []fibo=new int[initial];
        fibo[0]= 1;
        fibo[1]=1;
        for (int i = 2; i <= initial; i++) {
            fibo[i]=fibo[i-1]+fibo[i-2];
        }
        return fibo[initial-1];
    }
}