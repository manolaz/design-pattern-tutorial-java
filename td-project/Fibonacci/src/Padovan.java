public class Padovan implements  Algorithm {
    public Padovan() {
    };

    @Override
    public String getName() {
        return "Padovan";
    }

    @Override
    public double getVal(int initial) {
        //create array to store padovan values
        int []padv=new int[initial];
        padv[0]=padv[1]=padv[2]=1;
        for (int i = 3; i <= initial; i++) {
            padv[i]=padv[i-2]+padv[i-3];
        }
        return padv[initial-1];
    }
}
