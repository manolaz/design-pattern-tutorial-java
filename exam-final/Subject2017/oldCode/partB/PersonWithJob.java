package partB;

public class PersonWithJob extends PersonImpl {
    private String job;
    
    public PersonWithJob(String surname, String firstName, String job) {
        super(surname, firstName);
        this.job = job;
    }

    @Override
	public String toString() {
		// TODO Auto-generated method stub
        return String.format("%s %s %s", 
        this.surname(), this.firstName(), this.job);
	}

}