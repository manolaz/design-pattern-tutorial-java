package partB;

public class PersonImpl implements Person {

	private String surname;
	private String firstName;
	
	public PersonImpl(String surname, String firstName) {
		this.surname = surname;
		this.firstName = firstName;
	}
	
	public String surname() {
		return surname;
	}
	
	public String firstName() {
		return firstName;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.format("%s %s", surname(), firstName());
	}
}
