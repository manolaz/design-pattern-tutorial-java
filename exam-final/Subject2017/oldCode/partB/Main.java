package partB;

import javax.swing.JFrame;

import partB.PersonImpl;
import partB.DirectoryFrame;
public class Main {

	public static void main(String [] args) throws HomonymousException {
		PersonImpl p = new PersonImpl("Smith", "John");
		System.out.println(p);
		PersonImpl p3 = new PersonWithJob("Mio", "Daa", "Sniper");
		System.out.println(p3);
		
		Directory dir = new Directory(p,p3);
		JFrame f = new DirectoryFrame(dir);
		f.pack();
		f.setVisible(true);
	}
}
