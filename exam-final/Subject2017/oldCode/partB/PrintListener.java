package partB;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

import org.w3c.dom.Text;


public class PrintListener implements ActionListener {
	private JTextField text;
	private Directory dir;
	
	public PrintListener(JTextField text) {
		this.text = text;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		Person p =this.dir.person(text.getText());
		if (p != null) {
			System.out.println(p);
		}
	}


}
