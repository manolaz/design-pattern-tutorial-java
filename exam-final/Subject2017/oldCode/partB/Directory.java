package partB;
// HomonymousException;

import java.util.ArrayList;

public class Directory {
	private Person[] persons;

	public ArrayList<T>;
	
	public Directory(Person... persons) throws HomonymousException {
		// this.persons = persons;
		for(int i =0; i< persons.length; i++){
			for(int j = i+1; j< persons.length; j++){
				if (persons[i].equals(persons[j])) {
					throw new HomonymousException(persons[j]);
				}
			}
		}
		this.persons = persons.clone();
	}
	
	public Person person(String surname) {
		for (Person p : persons) {
			if (p.surname().equals(surname)) {
				return p;
			}
		}
		return null;
	}
}
