package part1;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class DirectoryFrame extends JFrame{
	public DirectoryFrame() {
		super("Directory");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(0, 1));
		JTextField text = new JTextField(10);
		panel.add(text);
		JButton b = new JButton("Ok");
		panel.add(b);
		b.addActionListener(new PrintListener(text));
		add(panel);
	}
}
