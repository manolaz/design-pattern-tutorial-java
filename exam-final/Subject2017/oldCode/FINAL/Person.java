package part2;

public interface Person {
	public String surname();

	public String firstName();
}
