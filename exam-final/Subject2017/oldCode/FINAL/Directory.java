package part2;

public class Directory {
	private Person[] persons;
	
	public Directory(Person... persons) {
		this.persons = persons;
	}
	
	public Person person(String surname) {
		for (Person p : persons) {
			if (p.surname().equals(surname)) {
				return p;
			}
		}
		return null;
	}
}
