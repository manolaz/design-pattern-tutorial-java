

public interface RequestHandler {

void setNextRequestHandler(RequestHandler requestHandler);

        boolean process(RequestData requestData);

}


public class DBDataHandler implements RequestHandler {

    private RequestHandler requestHandler;

    @Override

    public void setNextRequestHandler(RequestHandler requestHandler) {

        this.requestHandler = requestHandler;

    }

    @Override

    public boolean process(RequestData requestData) {

        requestData.setMetaDBData("Meta Data from DB is populated");

        return requestHandler == null ? true : requestHandler.process(requestData);

    }

}

public class FileDataHandler implements RequestHandler {

    private RequestHandler requestHandler;

    @Override

    public void setNextRequestHandler(RequestHandler requestHandler) {

        this.requestHandler = requestHandler;

    }

    @Override

    public boolean process(RequestData requestData) {

        requestData.setMetaFileData("Meta Data from File is populated");

        return requestHandler == null ? true : requestHandler.process(requestData);

    }

}


public class CloudDataHandler implements RequestHandler {

    private RequestHandler requestHandler;

    @Override

    public void setNextRequestHandler(RequestHandler requestHandler) {

        this.requestHandler = requestHandler;

    }

    @Override

    public boolean process(RequestData requestData) {

        requestData.setMetaCloudData("Meta Data from Cloud is populated");

        return requestHandler == null ? true : requestHandler.process(requestData);

    }

}



public class RequestData {

    private String metaDBData;

    private String metaFileData;

    private String metaCloudData;

    public String getMetaDBData() {

        return metaDBData;

    }

    public void setMetaDBData(String metaDBData) {

        this.metaDBData = metaDBData;

    }

    public String getMetaFileData() {

        return metaFileData;

    }

    public void setMetaFileData(String metaFileData) {

        this.metaFileData = metaFileData;

    }

    public String getMetaCloudData() {

        return metaCloudData;

    }

    public void setMetaCloudData(String metaCloudData) {

        this.metaCloudData = metaCloudData;

    }

    @Override

    public String toString() {

        return "RequestData [metaDBData=" + metaDBData + ",\n " +

            "metaFileData=" + metaFileData + ",\n metaCloudData=" +

            metaCloudData + "]";

    }

}


import java.util.*;

public class TestCOR {

    public static void main(String[] args) {

        RequestData requestData = new RequestData();

        List < RequestHandler > requestHandlers = new ArrayList < > ();

        requestHandlers.add(new DBDataHandler());

        requestHandlers.add(new FileDataHandler());

        requestHandlers.add(new CloudDataHandler());

        // create the chain of Handler

        for (int i = 0; i < requestHandlers.size() - 1; i++) {

            requestHandlers.get(i).setNextRequestHandler(requestHandlers.get(i + 1));

        }

        requestHandlers.get(0).process(requestData);

        System.out.println(requestData);

    }

}
